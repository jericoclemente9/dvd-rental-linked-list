#include <iostream>
#include <string>
#include <algorithm>
#include <bits/stdc++.h>
#include <windows.h>

using namespace std;
int main();
class List{
	private:
		typedef struct node{
			string name;
			string movie;
			string date;
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
	public:
		List();
		void AddNode(string name, string movie, string date);
		void DeleteNode(string delName);
		void PrintList();
		bool FindData(string name);
};

List::List(){
	head = NULL;
	curr = NULL;
	temp = NULL;
}

void List::AddNode(string name, string movie, string date){
	nodePtr n = new node;
	n->next = NULL;
	n->name = name;
	n->movie = movie;
	n->date = date;
	if(head != NULL){
		curr = head;
		while (curr->next != NULL){
			curr = curr->next;
		}
		curr->next = n;
	}
	else{
		head = n;
	}
	
}
void List::DeleteNode(string delName){
	nodePtr delPtr = NULL;
	temp = head;
	curr = head;
	while ((curr != NULL) && (curr->name != delName)){
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL){
		cout<<delName<<"was not in the List\n";
	}
	else{
		delPtr = curr;
		curr = curr->next;
		temp->next = curr;
		head = curr;
		delete delPtr;
	}
}

void List::PrintList(){
	curr = head;
	while (curr != NULL){
		cout<<"Name: "<<curr->name<<endl;
		cout<<"Movie: "<<curr->movie<<endl;
		cout<<"Date: "<<curr->date<<endl;
		curr = curr->next;
	}
}

bool List::FindData(string name){
	curr = head;
	while (curr && curr->name != name){
		curr = curr->next;
	}
	if (curr){
		return true;
	}
	else{
		return false;
	}
}


	List clist;
	string movie[25][5] = {
	{"Deadpool 1", "Tim Miller", "Ryan Reynolds", "Ryan Reynolds, Morena Baccarin, Ed Skrein, TJ Miller, Gina Carano, Brianna Hildebrand", "0"},
	{"Shazam!", "David Sandberg", "Peter Sefran", "Zachary Levi, Mark Strong, Asher Angel, Jack Grazer, Adam Brody, Djimon Hounsou", "0"},
	{"Alita: Battle Angel", "Robert Rodriguez", "James Cameron", "Rosa Salazar, Christoph Waltz, Jennifer Connelly, Mahershala Ali, Ed Skrein, Jackie Earle Haley, Keean Johnson", "0"},
	{"Happy Death Day 2U", "Christopher Landon", "Jason Blum", "Jessica Rothe, Israel Broussard, Suraj Sharma, Steve Zissis", "0"},
	{"Annabelle Comes Home", "Gary Dauberman", "James Wan", "McKenna Grace, Madison Iseman, Katie Sarife, Patrick Wilson, Vera Farmiga", "0"},
	{"Dumbo", "Tim Burton", "Justin Springer", "Collin Farrell, Michael Keaton, Danny DeVito, Eva Green, Alan Arkin", "0"},
	{"Pokemon Detective Pikachu", "Rob Letterman", "Mary Parent", "Ryan Reynolds, Justice Smith, Kathryn Newton, Suki Waterhouse, Omar Chaparro, Chris Geere, Ken Watanabe, Bill Nighy", "0"},
	{"John Wick: Chapter 3", "Chad Stahelski", "Basil Iwanyk", "Keanu Reeves, Halle Berry, Laurence Fishburne, Mark Dacascos, Asia Kate Dillon, Lance Reddick, Anjelica Huston, Ian McShane", "0"},
	{"Aladdin", "Guy Ritchie", "Dan Lin", "Will Smith, Mena Massoud, Naomi Scott, Marwan Kenzari, Navid Negahban, Nasim Pedrad, Billy Magnussen", "0"},
	{"Godzilla: King of the Monsters", "Michael Dougherty", "Mary Parent", "Kyle Chandler, Vera Farmiga, Millie Bobby Brown, Bradley Whitford, Sally Hawkins, Charles Dance, Thomas Middleditch, Aisha Hinds, O'Shea Jackson Jr., David Strathaim, Ken Watanabe, Zhang Ziyi", "0"},
	{"Spiderman: Far From Home", "Jon Watts", "Kevin Feige", "Tom Holland, Sammuel Jackson, Zendaya, Cobie Smulders, Jon Favreau, J.B. Smoove, Jacob Batalon, Martin Starr, Marisa Tomei, Jake Gyllenhaal", "0"},
	{"The Lion King", "Jon Favreau", "Jon Favreau", "Donald Glover, Seth Rogen, Chiwetel Ejiofor, Alfre Woodard, Billy Eichner, John Kani, John Oliver, Beyonce Knowles-Carter, James Earl Jones", "0"},
	{"Fantastic Beasts: The Crimes of Grindelwald", "David Yates", "J.K. Rowling", "Eddie Redmayne, Katherine Waterston, Dan Fogler, Alison Sudol, Ezra Miller, Zoe Kravitz, Callum Turner, Claudia Kim, William Nadylam, Kevin Guthrie, Jude Law, Johnny Depp", "0"},
	{"Jurassic World: Fallen Kingdom", "J.A. Bayona", "Frank Marshall", "Chris Pratt, Bryce Dallas Howard, Rafe Spall, Justice Smith, Daniella Pineda, James Cromwell, Toby Jones, Ted Levine, BD Wong, Isabella Sermon, Geraldine Chaplin, Jeff Goldblum", "0"},
	{"Fifty Shades Freed", "James Foley", "Michael De Luca", "Dakota Johnson, Jamie Dornan, Eric Johnson, Rita Ora, Luke Grimes, Victor Rasuk, Jennifer Ehle, Marcia Gay Harden", "0"},
	{"Ant-Man and The Wasp", "Peyton Reed", "Kevin Feige", "Paul Rudd, Evangeline Lilly, Michael Pe�a, Walton Goggins, Bobby Cannavale, Judy Geer, Tip Harris, David Dastmalchian, Hannah John-Kamen, Abby Ryder Fortson, Randall Park, Michelle Pfeiffer, Laurence Fishburne, Michael Douglas", "0"},
	{"Hotel Transylvania 3: Summer Vacation", "Genndy Tartakovsky", "Michelle Murdocca", "Adam Sandler, Andy Samberg, Selena Gomez, Kevin James, David Spade, Steve Buscemi, Keegan-Michael Key, Molly Shannon, Fran Drescher, Kathryn Hahn, Jim Gaffigan, Mel Brooks", "0"},
	{"Ralph Breaks The Internet", "Rich Moore", "Clark Spencer", "John Reilly, Sarah Silverman, Gal Gadot, Taraji Henson, Jack McBrayer, Jane Lynch, Alan Tudyk, Alfred Molina, Ed O'Neill", "0"},
	{"Maze Runner: The Death Cure", "Wes Ball", "Ellen Goldsmith-Vein", "Dylan O'Brien, Kaya Scodelario, Thomas Brodie-Sangster, Nathalie Emmanuel, Giancarlo Esposito, Aidan Gillen, Walton Goggins, Ki Hong Lee, Barry Pepper, Will Poulter, Patricia Clarkson", "0"},
	{"Ready Player One", "Steven Spielberg", "Donald De Line", "Tye Sheridan, Olivia Cooke, Ben Mendelsohn, T.J. Miller, Simon Pegg, Mark Rylance", "0"},
	{"The Nutcracker and the Four Realms", "Lasse Hallstr�m", "Mark Gordon", "Keira Knightley, Mackenzie Foy, Eugenio Derbez, Matthew Macfadyen, Richard Grant, Misty Copeland, Helen Mirren, Morgan Freeman", "0"},
	{"Incredibles 2", "Brad Bird", "John Walker", "Craig Nelson, Holly Hunter, Sarah Vowell, Huckleberry Milner, Samuel Jackson", "0"},
	{"Mission: Impossible-Fallout", "Christopher McQuarrie", "J.J. Abrams", "Tom Cruise, Henry Cavill, Ving Rhames, Simon Pegg, Rebecca Ferguson, Sean Harris, Angela Bassett, Michelle Monaghan, Alec Baldwin", "0"},
	{"Venom", "Ruben Fleischer", "Avi Arad", "Tom Hardy, Michelle Williams, Riz Ahmed, Scott Haze, Reid Scott", "0"},
	{"Insidious: The Last Key", "Adam Robitel", "Jason Blum", "Lin Shaye, Angus Sampson, Leigh Whannell, Spencer Locke, Caitlin Gerard, Bruce Davison", "0"}
	};

void View()
{
	char c;
	cout<<"Customers:"<<endl;
	clist.PrintList();
	cout<<"Any key to main: ";
	cin>>c;
	cin.ignore(1,'\n');
	if (c){
		main();
	}
}

void Return()
{
	string name;
	cout<<"Enter customer name:\n";
	getline(cin,name);
	bool x = clist.FindData(name);
	if (x){
		char c;
		cout<<"Entry found. Delete data?(y/n): ";
		cin>>c;
		cin.ignore(1,'\n');
		if (c=='y'||c=='Y'){
			clist.DeleteNode(name);
			cout<<"Entry deleted.";
			main();
		}
		else if(c=='n'||c=='N'){
			cout<<"Going back to main menu..";
			main();
		}
		else{
			cout<<"Invalid input";
			main();
		}
	}
	else{
		cout<<"Entry not found!";
		main();
	}
}

void Rent(int index)
{
	string name, title, date;
	title = movie[index][0];
	cout<<"Customer Name: ";
	getline(cin,name);
	cout<<"Date(MM/DD/YY): ";
	getline(cin,date);
	clist.AddNode(name,title,date);
	cout<<"Record Saved!";
	Sleep(1000);
	main();
}

void DisplayDetails(int index)
{
	system("cls");
	string stat = movie[index][4];
	cout<<"Details: "<<endl;
	cout<<"Title: "<<movie[index][0]<<endl;
	cout<<"Director: "<<movie[index][1]<<endl;
	cout<<"Producer: "<<movie[index][2]<<endl;
	cout<<"Actors: "<<movie[index][3]<<endl;
	if (stat == "0")
	{
		char c;
		cout<<"Status: Available"<<endl;
		cout<<"Rent?(y) ";
		cin>>c;
		cin.ignore(1,'\n');
		if (c=='y'||c=='Y'){
			movie[index][4] = "1";
			Rent(index);
		}
		else{
			cout<<"Going back to main menu...";
			main();
		}
	}
	else
	{
		cout<<"Status: Unavailable"<<endl;
	}
	Sleep(1000);
	main();
	
}

void DisplayMovies()
{
	cout<<"Movies:"<<endl;
	for (int i = 0; i < 25; i++){
		cout<<i+1<<". "<< movie[i][0]<<endl;
	}
	string mdetails;
	cout<<"Type movie to see details: "<<endl;
	getline(cin,mdetails);
	transform(mdetails.begin(),mdetails.end(), mdetails.begin(), ::tolower);
	for (int j = 0; j < 25; j++)
	{
		string x = movie[j][0];
		transform(x.begin(),x.end(),x.begin(),::tolower);
		if (x==mdetails){
			DisplayDetails(j);
			break;
		}
	}
	cout<<"Does not exist"<<endl;
	main();
}

int main()
{
	Sleep(1500);
	system("cls");
	char c;
	cout<<"Welcome!"<<endl;
	cout<<"1. View Movies 2. Return DVD 3. View Customers: ";
	cin>>c;
	cin.ignore(1,'\n');
	switch (c)
	{
		case '1': {
			DisplayMovies();
			break;
		}
		case '2': {
			Return();
			break;
		}
		case '3': {
			View();
			break;
		}
		default:{
			cout<<"Invalid input!";
			main();
			break;
		}
	}
}
